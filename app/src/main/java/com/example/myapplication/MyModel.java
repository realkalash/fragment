package com.example.myapplication;

import android.os.Parcel;
import android.os.Parcelable;

class MyModel implements Parcelable {
        String name;

    public String getName() {
        return name;
    }

    public MyModel(String name) {
        this.name = name;
    }

    protected MyModel(Parcel in) {
        name = in.readString();
    }

    public static final Creator<MyModel> CREATOR = new Creator<MyModel>() {
        @Override
        public MyModel createFromParcel(Parcel in) {
            return new MyModel(in);
        }

        @Override
        public MyModel[] newArray(int size) {
            return new MyModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
    }
}
