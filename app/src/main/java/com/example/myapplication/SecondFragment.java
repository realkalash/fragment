package com.example.myapplication;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


public class SecondFragment extends Fragment {

    private TextView one, two, three, four;
    private MyModel myModel;
    private String first;

    public static SecondFragment newInstance(String first, MyModel myModel) {

        Bundle args = new Bundle();

        args.putString("key", first);
        args.putParcelable("modelKey", myModel);

        SecondFragment fragment = new SecondFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();

        if (arg != null) {
            first = arg.getString("key", "");
            myModel = (MyModel) arg.getParcelable("modelKey"); //Получаем обьект из бандла
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_second, container, false);

        one = view.findViewById(R.id.id1);
        two = view.findViewById(R.id.id2);
        three = view.findViewById(R.id.id3);
        four = view.findViewById(R.id.id4);

        one.setText(first);
        two.setText(myModel.getName());

        return view;
    }

}
